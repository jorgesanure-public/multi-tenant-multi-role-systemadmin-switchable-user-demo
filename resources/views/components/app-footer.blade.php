@php
    $switchableUsers = auth()->user()->isSystemAdmin() ? \App\Models\User::with('tenants')->get() : [auth()->user()];
@endphp

@pushOnce('scripts')
<script>
    document.getElementById('user_tenant').addEventListener('change', function() {
        document.getElementById('switchable_tenant_form').submit();
    });
</script>
@endPushOnce

<footer class="bg-gray-800 text-white py-4 mt-auto flex items-center">
    <div class="ml-5">
        <form id="switchable_tenant_form" action="{{ route('switch.tenant') }}" method="post">
            @csrf
            <label for="user_tenant">Tenant</label>
            <select id="user_tenant" name="user_tenant" class="text-black">
                <option value="" disabled></option>
                @php
                    $currentUserTenant = auth()->id() . '-' . auth()->user()->current_tenant_id;
                @endphp
                @foreach ($switchableUsers as $user)
                    @foreach ($user->tenants as $tenant)
                    <option
                        @if ($currentUserTenant === $user->id . '-' . $tenant->id)
                            selected
                        @endif
                        value="{{ $user->id }}-{{ $tenant->id }}">
                        {{ $user->name }} - {{ $tenant->name }}
                    </option>
                    @endforeach
                @endforeach
            </select>
        </form>
    </div>
    <div class="w-full"></div>
    <div class="w-1/3 text-center">
      <p>&copy; 2024 Your Website. All rights reserved.</p>
    </div>
</footer>