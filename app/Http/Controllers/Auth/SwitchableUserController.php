<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Tenant;
use App\Models\User;
use App\Policies\TenantPolicy;
use Illuminate\Http\Request;

class SwitchableUserController extends Controller
{
    public function switchTenant(Request $request)
    {
        $userTenantId = $request->user_tenant;
        $userTenantIdArray = explode('-', $userTenantId);
        $userId = $userTenantIdArray[0];
        $tenantId = $userTenantIdArray[1];
        abort_if(empty($userId) || empty($tenantId), 404);

        // check tenant
        $this->authorize(TenantPolicy::SWITCH_TENANT, [Tenant::class, $tenantId, $userId]);

        $isSystemAdmin = auth()->user()->isSystemAdmin();
        if (! ($isSystemAdmin || auth()->user()->tenants()->where('tenants.id', $tenantId)->exists())) {
            abort(404);
        }

        if ($isSystemAdmin && auth()->id() != $userId) {
            auth()->login(User::where('id', $userId)->first() ?? abort(404));
        }

        // change tenant
        auth()->user()->update(['current_tenant_id' =>  $tenantId]);

        // return to dashboard
        return redirect()->route('dashboard');
    }
}
