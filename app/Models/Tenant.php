<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tenant extends Model
{
    use HasFactory;

    public const TENANT_NAME_SUFFIX = ' Team';

    protected $fillable = ['name'];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public static function buildTenantNameFromUserName(string $userName): string
    {
        return $userName . self::TENANT_NAME_SUFFIX;
    }
}
