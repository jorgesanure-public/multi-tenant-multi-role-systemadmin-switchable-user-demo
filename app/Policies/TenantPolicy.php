<?php

namespace App\Policies;

use App\Models\Tenant;
use App\Models\User;

class TenantPolicy
{
    public const SWITCH_TENANT = 'switchTenant';

    public function switchTenant(User $authUser, int $tenantId, int $tenantUserId)
    {
        return $authUser->isSystemAdmin() || ( $authUser->id === $tenantUserId && $authUser->tenants->contains(fn ($t) => $t->id === $tenantId) );
    }
}
