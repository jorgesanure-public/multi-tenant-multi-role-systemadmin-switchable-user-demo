<?php

namespace App\Enums;

enum RoleEnum: int
{
    case SYSTEM_ADMIN = 1;
}