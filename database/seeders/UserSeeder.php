<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use App\Models\Role;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (App::environment('local', 'development')) {
            $user = User::factory(1)
                ->state(fn ($attributes) => [
                    'email' => 'admin@email.com',
                    'password' => Hash::make('12341234'),
                ])
                ->create()->first();

            $user
                ->roles()
                ->attach(RoleEnum::SYSTEM_ADMIN->value);

            $tenants = Tenant::factory()
                ->count(3)
                ->create();
            
            $users = User::factory()
                ->count(4)
                ->create();

            foreach($users as $user) {
                $user->tenants()->attach($tenants->pluck('id'));
            }
        }
    }
}
